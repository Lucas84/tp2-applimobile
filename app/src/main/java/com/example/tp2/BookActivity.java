package com.example.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class BookActivity extends AppCompatActivity {

    private BookDbHelper bookdbhelper;
    private EditText titre;
    private TextView auteurs;
    private TextView annee;
    private TextView genre;
    private TextView editeur;
    private EditText editannee;
    private EditText editgenre;
    private EditText editediteur;
    private EditText editauteur;
    private Button sauvegarder;
    private Book book;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        bookdbhelper = new BookDbHelper(this);
        titre = (EditText) findViewById(R.id.nameBook);
        auteurs= (TextView) findViewById(R.id.labelAuthors);
        annee = (TextView) findViewById(R.id.labelYear);
        genre = (TextView) findViewById(R.id.labelGenres);
        editeur = (TextView) findViewById(R.id.labelPublisher);
        editauteur= (EditText) findViewById(R.id.editAuthors);
        editannee = (EditText) findViewById(R.id.editYear);
        editgenre = (EditText) findViewById(R.id.editGenres);
        editediteur = (EditText) findViewById(R.id.editPublisher);

        Intent intent = getIntent();
        book = intent.getParcelableExtra("book");
        if (book != null)
        {
            titre.setText(book.getTitle());
            editauteur.setText(book.getAuthors());
            editannee.setText(book.getYear());
            editgenre.setText(book.getGenres());
            editediteur.setText(book.getPublisher());
        }



        sauvegarder = (Button) findViewById(R.id.button);
        sauvegarder.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Sauvegarde(titre.getText().toString(),editauteur.getText().toString(),editannee.getText().toString(),editgenre.getText().toString(),editediteur.getText().toString());
            }
        });
    }
    public void Sauvegarde(String title,String authors, String year, String genres, String publisher){
        if (book == null)
        {
            Book book2 = new Book();
            book2.setTitle(title);
            book2.setAuthors(authors);
            book2.setYear(year);
            book2.setGenres(genres);
            book2.setPublisher(publisher);
            if(book2.getTitle().equals(""))
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(BookActivity.this);
                alert.setMessage("Sauvegarde impossible le titre doit être remplit");
                alert.setCancelable(true);
                alert.show();
            }
            else if(!bookdbhelper.addBook(book2))
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(BookActivity.this);
                alert.setMessage("Sauvegarde impossible ce titre existe déjà");
                alert.setCancelable(true);
                alert.show();
            }


        }
        else
        {
            book.setTitle(title);
            book.setAuthors(authors);
            book.setYear(year);
            book.setGenres(genres);
            book.setPublisher(publisher);
            bookdbhelper.updateBook(book);
        }

    }
}
