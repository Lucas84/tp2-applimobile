package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
   // private String books[] = {"Livre 1", "Livre 2", "Livre 3"};
    private BookDbHelper bookdbhelper;
    private Cursor cursor;
    private MyCursorAdapter cursoradapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bookdbhelper = new BookDbHelper(this);
        bookdbhelper.populate();
        cursor = bookdbhelper.fetchAllBooks();

        cursoradapter = new MyCursorAdapter(this, cursor, 1);

        ListView mListView = (ListView) findViewById(R.id.listView);
        mListView.setAdapter(cursoradapter);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (MainActivity.this,BookActivity.class);
                startActivity(intent);
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cursor.moveToPosition(position);
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                Book selectedBook = bookdbhelper.cursorToBook(cursor);
                intent.putExtra("book", selectedBook);
                startActivity(intent);
            }
        });

        mListView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.add(Menu.NONE, Menu.FIRST, Menu.NONE, "Supprimer");
            }

        });
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        cursor.moveToPosition(info.position);
        bookdbhelper.deleteBook(cursor);
        cursor = bookdbhelper.fetchAllBooks();
        cursoradapter.changeCursor(cursor);
        cursoradapter.notifyDataSetChanged();
        return true;

    }


}
